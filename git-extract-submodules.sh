#!/usr/bin/env bash
# Revert `git submodule absorbgitdirs`.
# Source: https://stackoverflow.com/a/51763718
# Notes:
# - Nested submodules must be moved first, so if you are specifying submodules manually, make sure the dependencies are correct
# - Assuming latest version of Git and GNU coreutils has been installed in the $PATH

gitextractsubmodules() {
    set -e
    pushd -- "$(git rev-parse --show-superproject-working-tree)" >/dev/null 2>&1
    >&2 echo "[*] Git base directory: $(pwd)"

    {
        if [ 0 -lt "$#" ]; then
            printf "%s\n" "$@"
        else
            # invert the results so nested submodules always comes first
            git submodule status --recursive | cut -d' ' -f3 | tac
        fi
    } | {
        local path
        while read -r path; do
            >&2 echo -n "[*] Processing submodule: ${path}..."
            if [ -f "${path}/.git" ]; then
                local git_dir
                git_dir="$(git -C "${path}" rev-parse --absolute-git-dir)"
                if [ -d "${git_dir}" ]; then
                    >&2 echo "migrate"
                    echo "MIGRATE ${path} ${git_dir}"

                    rm -f -- "${path}/.git"
                    mv --no-target-directory --backup=none -- "${git_dir}" "${path}/.git"
                    git --work-tree="${path}" --git-dir="${path}/.git" config --local --path --unset core.worktree
                    if 1>&- command -v attrib.exe; then
                        MSYS2_ARG_CONV_EXCL="*" attrib.exe "+H" "/D" "${path}/.git"
                    fi
                else
                    >&2 echo "source git directory not found"
                    echo "ERROR ${path}"
                fi
            else
                >&2 echo "already migrated"
                echo "SKIP ${path}"
            fi
        done
    }

    >&2 echo "[+] Done."
    popd >/dev/null 2>&1
}

gitextractsubmodules "$@"
